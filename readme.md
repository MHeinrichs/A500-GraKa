# A500-GraKa
 
This is an Zorro/Expansionport -expansion card for the big Amigas: A2000/300/4000 or A500 on the expansion port. It has a Picasso96-compatible RTG-driver and an automatic monitor switch. Picasso96 is currently owned by Individual Computers. Version 2.1 is on the aminet and Version 2.2 can be purchased at individual computers.

Here is a pic of the A500-version:
![Rev23-A500](https://gitlab.com/MHeinrichs/A500-GraKa/raw/master/Pics/Rev23.JPG)

The board is based on the PicassoII++ card originally developped by Georg Braun, who unfortunately left the Amiga community. 
The used VGA-Chip is a Cirrus GD5434 in ISA-mode and 2MB RAM.

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA OR HARDWARE!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of a Xilinx XC95144XL, the Cirrus GD5435, RAM and a monitor switch. The Amiga-Vidio signal is is looped through.

* The CPLD does three things:
    * It makes the Autoconfig for the card
    * It translated the ZorroII-Interface to the ISA-interface. Including a ugly quirk for 8bit-ISA acesses
    * It controlls the monitor switch

## What is in this repository?
This repository consists of several directories:

* Driver: Here is the driver. Cybergraphix-dirvers are experimental
* Layout: The board, schematic and bill of material (BOM) are in this directory. 
* Logic: Here the code, project files and pin-assignments for the CPLD is stored. There are two versions: 
* Pics: Some pics
* root directory: the readme

## How to install the driver?

1. Install a P96-distribution with manually activated PiccoloSD64-driver. You can donwload [version 2.0 from the Aminet](http://aminet.net/package/driver/video/Picasso96), Version 2.1 from the internet (ask google) or Version 2.2 from [Icomp](http://wiki.icomp.de/wiki/P96)
2. Now pick the [driver package](https://gitlab.com/MHeinrichs/A500-Graka/raw/master/Driver/GBAPIIplusplusDriver.zip) from this distribution
3. Unzip it and transfere it to the Amiga
4. Run the installer script. It comes with a VESA-compatible monitor setting
5. Set up the screen mode via ScreenMode and enjoy!
6. You can tweak your monitor setting via "Picasso96Mode"

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most smaller parts can be found on digikey or mouser or a parts supplier of your choice. However RAM, Cirrus and Xilinx are now obsolete and you need to find a source of your own. The ram is a generic 60ns 16bit*256k DRAM in J-Led package with two cas lines! 
A list of parts is found in the file [Partlist.xlsx](https://gitlab.com/MHeinrichs/A500-Graka/raw/master/Layout/Partlist.xlsx)

## How to programm the board?
The CPLD must be programmed via Xilinx IMPACT and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?t=40955). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

